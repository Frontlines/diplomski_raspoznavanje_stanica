class Config:
    # Data params
    path_to_train_images = "G:\\_ETFOS\\Diplomski\\DiplomskiRad\\Dataset\\WBC\\train"
    path_to_train_annotations = "G:\\_ETFOS\\Diplomski\\DiplomskiRad\\Dataset\\WBC\\train"
    path_to_eval_images = "G:\\_ETFOS\\Diplomski\\DiplomskiRad\\Dataset\\WBC\\valid"
    path_to_eval_annotations = "G:\\_ETFOS\\\Diplomski\\DiplomskiRad\\Dataset\\WBC\\valid"
    
    num_of_classes = 5
    min_img_size = 299
    max_img_size = 1024
    num_workers_train = 4
    num_worksers_eval = 4

    # AnchorGenerator
    anchor_sizes = (2, 4, 8, 16, 32)
    anchor_aspect_ratios = (0.5, 1.0, 2.0)

    # RPN params
    ##########Default Values##########
    #rpn_pre_nms_top_n_train=2000 
    #rpn_pre_nms_top_n_test=1000
    #rpn_post_nms_top_n_train=2000
    #rpn_post_nms_top_n_test=1000
    #rpn_nms_thresh=0.7
    #rpn_fg_iou_thresh=0.7
    #rpn_bg_iou_thresh=0.3
    #rpn_batch_size_per_image=256
    #rpn_positive_fraction=0.5
    ##########Default Values##########
    rpn_pre_nms_top_n_train=12000 
    rpn_pre_nms_top_n_test=6000
    rpn_post_nms_top_n_train=2000
    rpn_post_nms_top_n_test=300
    rpn_nms_thresh=0.7
    rpn_fg_iou_thresh=0.7
    rpn_bg_iou_thresh=0.3
    rpn_batch_size_per_image=256
    rpn_positive_fraction=0.5
    
    # Box Params
    ##########Default Values##########
    #box_score_thresh=0.05
    #box_nms_thresh=0.5
    #box_detections_per_img=100
    #box_fg_iou_thresh=0.5
    #box_bg_iou_thresh=0.5
    #box_batch_size_per_image=512
    #box_positive_fraction=0.25
    ##########Default Values##########
    box_score_thresh=0.05
    box_nms_thresh=0.5
    box_detections_per_img=100
    box_fg_iou_thresh=0.5
    box_bg_iou_thresh=0.1
    box_batch_size_per_image=256
    box_positive_fraction=0.25

    # Training params
    batch_size = 4
    num_epochs = 101

    # Optimizer params
    ##########Default Values##########
    #learning_rate = 0.0003
    #momentum = 0.9
    #weight_decay = 0.0005
    #gamma = 0.1
    ##########Default Values##########
    learning_rate = 0.001
    momentum = 0.9
    weight_decay = 0.0001
    gamma = 0.1 # if using StepLR

    # pretrained model path (for transfer learning)
    # you should consider freezing some layers or setting feature_extracting to True
    pretrained_load_path = None
    # load saved checkpoint
    checkpoint_load_path = None
    # save trained model
    save_path = "G:\_ETFOS\Diplomski\DiplomskiRad"
    checkpoint_period = 50 # save every n-th epoch
    
    # Faster RCNN backbone - can be VGG, MobileNetV2, ResNet50, InceptionV3
    backbone_type = "MobileNetV2"
    # VGG has 24 layers for feature detection
    # MobileNetV2 has 156 layers for feature detection
    # Resnet has 9 layers for feature detection
    # InceptionV3 has 33 layers for feature detection (nisam provjerio)
    num_of_freeze_layers = 0
    # setting this to True will ignore num_of_freeze_layers
    feature_extracting = False 
