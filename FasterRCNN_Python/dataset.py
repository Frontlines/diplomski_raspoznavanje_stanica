import torch
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
from torchvision import datasets, transforms
from PIL import Image
from xml.dom.minidom import parse

class WBCDataset(torch.utils.data.Dataset):
    classifications = {'Lymphocyte' : 1,
                       'Neutrophil' : 2,
                       'Eosinophil' : 3,
                       'Monocyte' : 4,
                       'Basophil' : 5
                       }

    def __init__(self, imagesPath, annotationsPath, transforms=None):
        # transformations to be applied per image
        self.transforms = transforms
        # load all image files, sorting them to ensure that they are aligned
        self.images_path = imagesPath
        files = list(sorted(os.listdir(imagesPath)))
        # take into account files that are .jpg or .png (images)
        self.images = [fname for fname in files if fname.endswith('.jpg') or fname.endswith('.png')]
        
        #load all annotations and sort them too
        self.annotations_path = annotationsPath
        files = list(sorted(os.listdir(annotationsPath)))
        self.bboxXmls = [fname for fname in files if fname.endswith('.xml')]

    def __getitem__(self, index):
        # load images and bbox xml
        img_path = os.path.join(self.images_path, self.images[index])
        bbox_xml_path = os.path.join(self.annotations_path, self.bboxXmls[index])
        img = Image.open(img_path).convert("RGB")

        # read Pascal VOC bounding box file
        dom = parse(bbox_xml_path)

        # get objects from xml
        data = dom.documentElement
        objects = data.getElementsByTagName('object')

        # get bounding box coordinates and labels
        boxes = []
        labels = []
        for obj in objects:
            bndbox = obj.getElementsByTagName('bndbox')[0]
            xmin = np.float(bndbox.getElementsByTagName('xmin')[0].childNodes[0].nodeValue)
            ymin = np.float(bndbox.getElementsByTagName('ymin')[0].childNodes[0].nodeValue)
            xmax = np.float(bndbox.getElementsByTagName('xmax')[0].childNodes[0].nodeValue)
            ymax = np.float(bndbox.getElementsByTagName('ymax')[0].childNodes[0].nodeValue)

            width = xmax - xmin
            height = ymax - ymin

            # skip invalid bounding boxes
            if(width <= 0.0 or height <= 0.0):
                continue

            name = obj.getElementsByTagName('name')[0].childNodes[0].nodeValue.rstrip()
            labels.append(self.classifications[name])
            boxes.append([xmin, ymin, xmax, ymax])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)

        image_id = torch.tensor([index])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        # COCO eval metrics require a iscrow param, so just spoof the value as zero
        iscrowd = torch.zeros((len(objects),), dtype=torch.int64)
 
        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd
 
        # apply transformations
        if self.transforms is not None:
            img, target = self.transforms(img, target)
 
        return img, target

    def __len__(self):
        return len(self.images)

class CellsDataset(torch.utils.data.Dataset):
    classifications = {'RBC' : 1,
                       'WBC' : 2,
                       'Platelets' : 3
                       }

    def __init__(self, imagesPath, annotationsPath, transforms=None):
        self.transforms = transforms
        
        self.images_path = imagesPath
        files = list(sorted(os.listdir(imagesPath)))
        self.images = [fname for fname in files if fname.endswith('.jpg') or fname.endswith('.png')]
        
        self.annotations_path = annotationsPath
        files = list(sorted(os.listdir(annotationsPath)))
        self.bboxXmls = [fname for fname in files if fname.endswith('.xml')]

    def __getitem__(self, index):
        # load images and bbox xml
        img_path = os.path.join(self.images_path, self.images[index])
        bbox_xml_path = os.path.join(self.annotations_path, self.bboxXmls[index])
        img = Image.open(img_path).convert("RGB")

        # read Pascal VOC bounding box file
        dom = parse(bbox_xml_path)

        # get objects from xml
        data = dom.documentElement
        objects = data.getElementsByTagName('object')

        # get bounding box coordinates and labels
        boxes = []
        labels = []
        for obj in objects:
            bndbox = obj.getElementsByTagName('bndbox')[0]
            xmin = np.float(bndbox.getElementsByTagName('xmin')[0].childNodes[0].nodeValue)
            ymin = np.float(bndbox.getElementsByTagName('ymin')[0].childNodes[0].nodeValue)
            xmax = np.float(bndbox.getElementsByTagName('xmax')[0].childNodes[0].nodeValue)
            ymax = np.float(bndbox.getElementsByTagName('ymax')[0].childNodes[0].nodeValue)

            width = xmax - xmin
            height = ymax - ymin

            # skip invalid bounding boxes
            if(width <= 0.0 or height <= 0.0):
                continue

            name = obj.getElementsByTagName('name')[0].childNodes[0].nodeValue.rstrip()
            labels.append(self.classifications[name]) 
            boxes.append([xmin, ymin, xmax, ymax])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)

        image_id = torch.tensor([index])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        iscrowd = torch.zeros((len(objects),), dtype=torch.int64)
 
        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd
 
        if self.transforms is not None:
            img, target = self.transforms(img, target)
 
        return img, target

    def __len__(self):
        return len(self.images)