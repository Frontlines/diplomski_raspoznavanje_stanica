import torch
import torchvision
import os
import argparse
import detection.utils as utils
from detection.engine import train_one_epoch, evaluate
from dataset import WBCDataset, CellsDataset
from config import Config
from FasterRCNN_Model import get_object_detection_model, freeze_layers
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
import detection.transforms as T
import onnx
import onnxruntime

def get_transform(train):
    transforms = []
    transforms.append(T.ToTensor())
    return T.Compose(transforms)

def train(cfg):
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # use our dataset and defined transformations
    # if training to detect blood cell type (WBC, RBC, Platelets) use CellsDataset, if training to detect White Blood Cell type use WBCDataset
    dataset_train = CellsDataset(cfg.path_to_train_images, cfg.path_to_train_annotations, get_transform(train=False))
    dataset_eval = CellsDataset(cfg.path_to_eval_images, cfg.path_to_eval_annotations, get_transform(train=False))

    # define training and validation data loaders
    # num of workers can be changed in config (add this)
    data_loader_train = torch.utils.data.DataLoader(
        dataset_train, batch_size=cfg.batch_size, shuffle=True, num_workers=cfg.num_workers_train,
        collate_fn=utils.collate_fn)

    data_loader_eval = torch.utils.data.DataLoader(
        dataset_eval, batch_size=1, shuffle=False, num_workers=cfg.num_worksers_eval,
        collate_fn=utils.collate_fn)

    # get the model using our helper function
    model = get_object_detection_model(cfg)

    # load pretrained model if pretrained model exists
    if(cfg.pretrained_load_path):
        # get the number of input features for the classifier
        in_features = model.roi_heads.box_predictor.cls_score.in_features

        model.roi_heads.box_predictor = None

        model.load_state_dict(torch.load(cfg.pretrained_load_path), strict=False)

        # replace the pre-trained head with a new one
        model.roi_heads.box_predictor = FastRCNNPredictor(in_features, cfg.num_of_classes + 1)

        # freeze backbone layers here
        model = freeze_layers(model, cfg.backbone_type, cfg.num_of_freeze_layers, cfg.feature_extracting)

    # move model to the right device (CPU or GPU)
    model.to(device)

    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=cfg.learning_rate,
                                momentum=cfg.momentum, weight_decay=cfg.weight_decay)

    # load checkpoint if checkpoint is supplied
    starting_epoch = 0
    if(cfg.checkpoint_load_path):
        checkpoint = torch.load(cfg.checkpoint_load_path)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        starting_epoch = checkpoint['epoch']

    # construct learning rate scheduler
    #lr_scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=[100,250,500,1000], gamma=0.1)
    #lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.1, last_epoch=-1, verbose=False)
    lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0=1, T_mult=2)

    for epoch in range(starting_epoch, cfg.num_epochs):
        # train for one epoch, printing every 10 iterations
        train_one_epoch(model, optimizer, data_loader_train, device, epoch, print_freq=10)
        # update the learning rate
        lr_scheduler.step()
        # evaluate on the test dataset
        evaluate(model, data_loader_eval, device=device)

        if(epoch % cfg.checkpoint_period == 0):
            torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict()
            }, os.path.join(cfg.save_path, "cellsModel_ckpt{}.tar".format(epoch)))
            print('')
            print('==================================================')
            print('')
            print("Saved checkpoint.")

        print('')
        print('==================================================')
        print('')

    print("Training complete!")

    # set the model to evaluation mode when saving the model to freeze layers
    # generally to prep the model for inference instead of train
    model.eval()
    img, labels = dataset_eval[0]

    # save the pytorch model, well the state dicts
    torch.save(model.state_dict(), os.path.join(cfg.save_path, "cellsModel_final.pt"))
    print("Saved pytorch model.")

    onnx_path = os.path.join(cfg.save_path, "cellsModel_final.onnx")
    torch.onnx.export(model,                # model being run
              [img.to(device)],             # model input (or a tuple for multiple inputs)
              onnx_path,                    # where to save the model (can be a file or file-like object)
              export_params=True,           # store the trained parameter weights inside the model file
              opset_version=11,             # the ONNX version to export the model to
              do_constant_folding=True,     # whether to execute constant folding for optimization
              input_names = ['image_in'],   # the model's input names
              output_names = ['bboxes', 'labels', 'scores'] # the model's output names
              )

    print("Saved onnx model.")
    print("Checking onnx model validity.")

    onnx_model = onnx.load(onnx_path)
    onnx.checker.check_model(onnx_model)
    print("Model is valid.")
    print("Done training.")

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--train_images_path", required=True, dest='train_images_path')
    parser.add_argument("--train_annotations_path", required=True, dest='train_annotations_path')
    parser.add_argument("--eval_images_path", required=True, dest='eval_images_path')
    parser.add_argument("--eval_annotations_path", required=True, dest='eval_annotations_path')
    parser.add_argument("--checkpoint_path", required=False, default='', dest='checkpoint_path')
    parser.add_argument("--pretrained_path", required=False, default='', dest='pretrained_path')
    parser.add_argument("--save_path", required=True, dest='save_path')
    parser.add_argument("--checkpoint_steps", required=False, type=int, default=300, dest='checkpoint_steps')
    parser.add_argument("--num_of_classes", required=True, type=int, dest='num_of_classes')
    parser.add_argument("--num_of_steps", required=False, type=int, dest='num_steps')
    parser.add_argument("--batch_size", required=False, type=int, default=4, dest='batch_size')
    parser.add_argument("--feature_extracting", required=False, type=bool, default=False, dest='feature_extracting')
    args = parser.parse_args()

    # init config
    conf = Config()
    conf.path_to_train_images = args.train_images_path
    conf.path_to_train_annotations = args.train_annotations_path
    conf.path_to_eval_images = args.eval_images_path
    conf.path_to_eval_annotations = args.eval_annotations_path
    conf.checkpoint_load_path = args.checkpoint_path
    conf.pretrained_load_path = args.pretrained_path
    conf.save_path = args.save_path
    conf.checkpoint_period = args.checkpoint_steps
    conf.num_of_classes = args.num_of_classes
    conf.num_epochs = args.num_steps
    conf.batch_size = args.batch_size
    conf.feature_extracting = args.feature_extracting

    train(conf)