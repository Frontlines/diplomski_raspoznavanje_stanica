import torch
import torchvision
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor, FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator

# The CNN (Convolutional Neural Network) to use for feature detection
# We only use the feature extraction layers and discard the classification layer/s.
# It's important to assigned the output channel count for the backbone, else training will fail.
def get_backbone(backbone_type, layer_freeze_count, feature_extracting, use_pretrained = True):
    backbone = None

    if(backbone_type == "VGG"):
        model = torchvision.models.vgg16(use_pretrained)

        if feature_extracting:
            for param in model.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 24)
            ct = 0
            features = model.features
            for name, param in features.named_parameters():
                if ct < layer_freeze_count:
                    param.requires_grad = False
                ct += 1
        
        backbone = model.features
        backbone.out_channels = 512
        print("VGG backbone selected.")
    elif (backbone_type == "MobileNetV2"):
        model = torchvision.models.mobilenet_v2(use_pretrained)

        if feature_extracting:
            for param in model.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 156)
            ct = 0
            features = model.features
            for name, param in features.named_parameters():
                if ct < layer_freeze_count:
                    param.requires_grad = False
                ct += 1

        backbone = model.features
        backbone.out_channels = 1280
        print("MobileNetV2 backbone selected.")
    elif (backbone_type == "ResNet50"):
        model = torchvision.models.resnet50(use_pretrained)

        if feature_extracting:
            for param in model.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 9)
            ct = 0
            for child in model.children():
                if ct < layer_freeze_count:
                    child.requires_grad = False
                ct += 1

        modules = list(model.children())[:-1]
        backbone = torch.nn.Sequential(*modules)
        backbone.out_channels = 2048
        print("ResNet50 backbone selected.")
    elif (backbone_type == "InceptionV3"):
        model = torchvision.models.inception_v3(use_pretrained, aux_logits=False)

        if feature_extracting:
            for param in model.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 9)
            ct = 0
            for child in model.children():
                if ct < layer_freeze_count:
                    child.requires_grad = False
                ct += 1

        modules = list(model.children())[:-1]
        backbone = torch.nn.Sequential(*modules)
        backbone.out_channels = 2048
        print("InceptionV3 backbone selected.")
    else:
        backbone = torchvision.models.vgg16(use_pretrained).features
        backbone.out_channels = 512
        print("VGG backbone selected.")

    return backbone

# Use for freezing backbone layers in a trained model
def freeze_layers(model, backbone_type, layer_freeze_count, feature_extracting):
    if(backbone_type == "VGG"):
        if feature_extracting:
            for param in model.backbone.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 24)
            ct = 0
            for param in model.backbone.parameters():
                if ct < layer_freeze_count:
                    param.requires_grad = False
                ct += 1
    elif (backbone_type == "MobileNetV2"):
        if feature_extracting:
            for param in model.backbone.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 156)
            ct = 0
            for param in model.backbone.parameters():
                if ct < layer_freeze_count:
                    param.requires_grad = False
                ct += 1
    elif (backbone_type == "ResNet50"):
        if feature_extracting:
            for param in model.backbone.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 9)
            ct = 0
            for child in model.backbone.parameters():
                if ct < layer_freeze_count:
                    child.requires_grad = False
                ct += 1
    elif (backbone_type == "InceptionV3"):
        if feature_extracting:
            for param in model.backbone.parameters():
                param.requires_grad = False
        else:
            layer_freeze_count = min(max(layer_freeze_count, 0), 33)
            ct = 0
            for child in model.backbone.parameters():
                if ct < layer_freeze_count:
                    child.requires_grad = False
                ct += 1

    return model

def get_object_detection_model(config):
    # load feature extraction layers for a given backbone type
    print("Generating backbone.")
    backbone = get_backbone(config.backbone_type, config.num_of_freeze_layers, config.feature_extracting)

    # Generate 5 x 3 anchors per spatial location for our RPN
    # Spatial locations are locations extracted from the CNN backbone (feature maps)   
    print("Constructing Anchor Generator")
    anchor_generator = AnchorGenerator(sizes=(config.anchor_sizes,),
                                       aspect_ratios=(config.anchor_aspect_ratios,))

    # Region of Interest cropping, as well as
    # the size of the crop after rescaling (size of crop is (7,7) per region).
    print("Constructing RoI")
    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                    output_size=7,
                                                    sampling_ratio=2)

    # configure our FasterRCNN model
    print("Building fresh Faster RCNN model")
    model = FasterRCNN(backbone,
                       num_classes=config.num_of_classes + 1,
                       min_size=config.min_img_size,
                       max_size=config.max_img_size,
                       rpn_anchor_generator=anchor_generator,
                       rpn_pre_nms_top_n_train=config.rpn_pre_nms_top_n_train,
                       rpn_pre_nms_top_n_test=config.rpn_pre_nms_top_n_test,
                       rpn_post_nms_top_n_train=config.rpn_post_nms_top_n_train,
                       rpn_post_nms_top_n_test=config.rpn_post_nms_top_n_test,
                       rpn_nms_thresh=config.rpn_nms_thresh,
                       rpn_fg_iou_thresh=config.rpn_fg_iou_thresh,
                       rpn_bg_iou_thresh=config.rpn_bg_iou_thresh,
                       rpn_batch_size_per_image=config.rpn_batch_size_per_image,
                       rpn_positive_fraction=config.rpn_positive_fraction,
                       box_score_thresh=config.box_score_thresh,
                       box_nms_thresh=config.box_nms_thresh,
                       box_detections_per_img=config.box_detections_per_img,
                       box_fg_iou_thresh=config.box_fg_iou_thresh,
                       box_bg_iou_thresh=config.box_bg_iou_thresh,
                       box_batch_size_per_image=config.box_batch_size_per_image,
                       box_positive_fraction=config.box_positive_fraction,
                       box_roi_pool=roi_pooler)
 
    return model