# -*- coding: utf-8 -*-
import cv2
import numpy as np
import sys
from os import walk, getenv, mkdir

# saves image to save path with given name
def save(path, image):
    print("Saving image to: " + path)
    cv2.imwrite(path, image)

# loads an image from path
def load(path, flag):
    return cv2.imread(path, flag)

if(len(sys.argv) < 4):
    print('usage: python ChangeImageFileFormat.py "file format (.png,.jpg, etc.)" "direktorij sa slikama" "direktorij za spremanje"')
    sys.exit()

try:
    FILE_FORMAT = sys.argv[1]
    PATH_TO_IMAGES = sys.argv[2]
    SAVE_PATH = sys.argv[3]
except:
    print('supply parameters as said in usage')
    sys.exit()

dirTree = walk(PATH_TO_IMAGES)

for tup in dirTree:
    for filename in tup[2]:
        imgPath = tup[0] + "\\" + filename

        image = load(imgPath, cv2.IMREAD_COLOR)

        if image is None:
            print('Error - Nepodrzan format slike')
            print('Podrzani formati:\n.bmp, .dib, .jpeg, .jpg, .jpe, .jp2, .png, .webp, .pbm, .pgm, .ppm, .sr, .ras, .tiff, .tif')
            continue

        print("Successfully loaded image " + filename)

        fns = filename.split('.')
        fnj = ""
        for i in range(0, len(fns) - 1):
            fnj += fns[i] + "."

        save(SAVE_PATH + '\\' + fnj + FILE_FORMAT[1:], image)

        print('Image saved as ' + filename)