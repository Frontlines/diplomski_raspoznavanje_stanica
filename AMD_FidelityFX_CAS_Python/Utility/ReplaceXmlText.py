# -*- coding: utf-8 -*-#
import lxml.etree as ET
import sys
from os import walk, getenv, mkdir

def replaceTextInXml(path):
    #adding the encoding when the file is opened and written is needed to avoid a charmap error
    with open(path, encoding="utf8") as f:
      tree = ET.parse(f)
      root = tree.getroot()

      for elem in root.getiterator():
        try:
          elem.text = elem.text.replace('.tif', '.png')
        except AttributeError:
          pass

    #tree.write('output.xml', encoding="utf8")
    # Adding the xml_declaration and method helped keep the header info at the top of the file.
    tree.write(path, xml_declaration=True, method='xml', encoding="utf8")

if(len(sys.argv) < 2):
    print('usage: python ReplaceXmlText.py "direktorij sa xml datotekama"')
    sys.exit()

try:
    PATH_TO_XML = sys.argv[1]
except:
    print('supply parameters as said in usage')
    sys.exit()

dirTree = walk(PATH_TO_XML)

for tup in dirTree:
    for filename in tup[2]:
        xmlFilePath = tup[0] + "\\" + filename
        replaceTextInXml(xmlFilePath)
        print('File saved')


