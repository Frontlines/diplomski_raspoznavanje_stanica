import numpy as np
import cv2
import pandas as pd
import sys
from os import walk, getenv, mkdir
import errno

def clamp(val, minVal, maxVal):
    return min(maxVal, max(minVal, val))

def getCrop(image, x, y, width, height):
    imgHeight = image.shape[0]
    imgWidth = image.shape[1]

    cropWidth = width * imgWidth
    cropHeight = height * imgHeight

    absX = int(x * imgWidth)
    absY = int(y * imgHeight)

    xmin = clamp(int(absX - cropWidth//2), 0, imgWidth)
    xmax = clamp(int(absX + cropWidth//2), 0, imgWidth)
    ymin = clamp(int(absY - cropHeight//2), 0, imgHeight)
    ymax = clamp(int(absY + cropHeight//2), 0, imgHeight)

    return image[ymin : ymax, xmin : xmax]

if __name__ == '__main__':
    if(len(sys.argv) < 4):
        print('usage: python CropImageBoundingBoxes.py "path do datoteke sa klasama" "path do annotacija" "path do slikama" "path za spremanje cropanih slika"')
        sys.exit()

    try:
        PATH_TO_CLASSES = sys.argv[1]
        PATH_TO_ANNOTATIONS = sys.argv[2]
        PATH_TO_IMAGES = sys.argv[3]
        SAVE_PATH = sys.argv[4]
    except:
        print('supply parameters as said in usage')
        sys.exit()

    dirTreeAnnotations = walk(PATH_TO_ANNOTATIONS)
    dirTreeImages = walk(PATH_TO_IMAGES)
    savePath = SAVE_PATH

    try:
        mkdir(savePath)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass

    annotationDict = {
        "filename": [],
        "width": [],
        "height": [],
        "class": [],
        "xmin": [],
        "ymin": [],
        "xmax": [],
        "ymax": [],
        }

    classes = pd.read_csv(PATH_TO_CLASSES, names=["label"])
    print("Classes")
    print(classes)

    # iterate through the annotations
    for tup in dirTreeAnnotations:
        for filename in tup[2]:
            if (not(filename.endswith(".txt"))):
                continue
            
            imgName = filename[:-4] + ".png"

            # read the annotation for the image
            adf = pd.read_csv(tup[0] + "\\" + filename, sep=' ', names=["x", "y", "width", "height"], index_col=0)
            # remove rows that are labeled as RBC, WBC and Platelets
            adf = adf.drop(range(0,3), errors='ignore')
            
            # construct image path to read
            imgPath = PATH_TO_IMAGES + "\\" + imgName

            # read image in path
            image = cv2.imread(imgPath, cv2.IMREAD_COLOR)

            if image is None:
                print('Error - Nepodrzan format slike')
                print('Podrzani formati:\n.bmp, .dib, .jpeg, .jpg, .jpe, .jp2, .png, .webp, .pbm, .pgm, .ppm, .sr, .ras, .tiff, .tif')
                continue

            print("Successfully loaded image " + filename)

            # iterate through all the bouding boxes and save them as individual 
            # also save the label and image name in new pandas array
            cropIndex = 0
            for index, data in adf.iterrows():
                cropName = filename[:-4] + "_crop{0}.png".format(cropIndex)
                crop = getCrop(image, data['x'], data['y'], data['width'], data['height'])
                cv2.imwrite(savePath + "\\" + cropName, crop)
                annotationDict['filename'].append(cropName)
                annotationDict['width'].append(crop.shape[1])
                annotationDict['height'].append(crop.shape[0])
                annotationDict['class'].append(classes.loc[index, 'label'])
                annotationDict['xmin'].append(0)
                annotationDict['ymin'].append(0)
                annotationDict['xmax'].append(crop.shape[1])
                annotationDict['ymax'].append(crop.shape[0])
                cropIndex += 1

            print('OK - image processed: ' + filename)

cropLabelsDF = pd.DataFrame(annotationDict)
cropLabelsDF.to_csv(savePath + "\\_annotations.csv", header=True, index=False)