# -*- coding: utf-8 -*-
import sys
from os import walk, getenv, mkdir

if(len(sys.argv) < 3):
    print('usage: python ReplaceFirstValue.py "direktorij sa textualnim datotekama" "direktorij za spremanje"')
    sys.exit()

try:
    PATH_TO_FILES = sys.argv[1]
    SAVE_PATH = sys.argv[2]
except:
    print('supply parameters as said in usage')
    sys.exit()

dirTree = walk(PATH_TO_FILES)

for tup in dirTree:
    for filename in tup[2]:
        filePath = tup[0] + "\\" + filename

        lines = []
        for line in open(filePath, 'r'):
            if (line[0] == '0'):
                lines.append(line.replace('0', '2', 1))
            elif (line[0] == '1'):
                lines.append(line.replace('1', '0', 1))
            elif (line[0] == '2'):
                lines.append(line.replace('2', '1', 1))
            else:
                lines.append(line)
            

        print ('\n'.join(lines))

        file = open(SAVE_PATH + "\\" + filename, 'w')
        file.writelines(lines)
        print('File saved as ' + filename)
