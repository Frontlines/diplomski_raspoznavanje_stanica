# -*- coding: utf-8 -*-
import numpy as np
import cv2
import sys
import AMD_FidelityFX_CAS_Python
from os import walk, getenv, mkdir
import errno

sharpenKernel = np.array([
    [0,-1,0],
    [-1,5,-1],
    [0,-1,0]])

boxblurKernel = np.array([
    [1,1,1],
    [1,1,1],
    [1,1,1]]) * (1/9)

unsharpenKernel = np.array([
    [1,4,6,4,1],
    [4,16,24,16,4],
    [6,24,-476,24,6],
    [4,16,24,16,4],
    [1,4,6,4,1]]) * (-1/256)

methodNames = {1: "SharpenFilter",
           2: "BoxblurFilter",
           3: "UnsharpenFilter",
           4: "MedianFilter",
           5: "GaussFilter",
           6: "BilateralFilter",
           7: "AMD_FidelityFX_CAS"}

# saves image to save path with given name
def save(path, image):
    print("Saving image to: " + path)
    cv2.imwrite(path, image)

# loads an image from path
def load(path, flag):
    return cv2.imread(path, flag)

def processImage(image, method):
    if(method == 1):
        # sharpen kernel
        global sharpenKernel
        return cv2.filter2D(image, -1, sharpenKernel, borderType=cv2.BORDER_REPLICATE)
    elif(method == 2):
        # box blur kernel
        global boxblurKernel
        return cv2.filter2D(image, -1, boxblurKernel, borderType=cv2.BORDER_REPLICATE)
    elif(method == 3):
        # unsharpen kernel
        global unsharpenKernel
        return cv2.filter2D(image, -1, unsharpenKernel, borderType=cv2.BORDER_REPLICATE)
    elif(method == 4):
        # median blur
        return cv2.medianBlur(image, 3)
    elif(method == 5):
        # gauss blur
        return cv2.GaussianBlur(image, (5,5), 0, borderType=cv2.BORDER_REPLICATE)
    elif(method == 6):
        # bilateral filter
        return cv2.bilateralFilter(image, 13, 150, 150, borderType=cv2.BORDER_REPLICATE)
    elif(method == 7):
        # AMD FidelityFX CAS
        return AMD_FidelityFX_CAS_Python.casFilter_async(image, 1.0)
    else:
        print("No method with such index")
        return image

if __name__ == '__main__':
    if(len(sys.argv) < 5):
        print('usage: python KernelImageProcessing.py <method> <num of times to process> "absolute path do slikama" "absolute path za spremanje novih slika"')
        print("method id's: 1 - sharpen, 2 - box blur, 3 - unsharpen, 4 - median blur, 5 - gauss blur, 6 - bilateral filter, 7 - AMD FidelityFX CAS")
        sys.exit()

    try:
        METHOD = int(sys.argv[1])
        PROCESS_COUNT = int(sys.argv[2])
        PATH_TO_IMAGES = sys.argv[3]
        SAVE_PATH = sys.argv[4]
    except:
        print('supply parameters as said in usage')
        sys.exit()

    dirTree = walk(PATH_TO_IMAGES)
    savePath = SAVE_PATH + "\\" + methodNames[METHOD] + "_x{0}".format(PROCESS_COUNT)

    try:
        mkdir(savePath)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass

    for tup in dirTree:
        for filename in tup[2]:
            imgPath = tup[0] + "\\" + filename

            image = load(imgPath, cv2.IMREAD_COLOR)

            if image is None:
                print('Error - Nepodrzan format slike')
                print('Podrzani formati:\n.bmp, .dib, .jpeg, .jpg, .jpe, .jp2, .png, .webp, .pbm, .pgm, .ppm, .sr, .ras, .tiff, .tif')
                continue

            print("Successfully loaded image " + filename)

            for pc in range(0, PROCESS_COUNT):
                image = processImage(image, METHOD)
            
            save(savePath + '\\' + filename, image)
            print('OK - image processed: ' + filename)