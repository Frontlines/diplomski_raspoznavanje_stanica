# -*- coding: utf-8 -*-
import cv2
import numpy as np
import sys
from os import walk, getenv, mkdir

# saves image to save path with given name
def save(path, image):
    print("Saving image to: " + path)
    cv2.imwrite(path, image)

# loads an image from path
def load(path, flag):
    return cv2.imread(path, flag)

if __name__ == '__main__':
    if(len(sys.argv) < 4):
        print('usage: python ResizeImages.py <factor> "absolute path do slikama" "absolute path za spremanje novih slika"')
        sys.exit()

    try:
        FACTOR = float(sys.argv[1])
        PATH_TO_IMAGES = sys.argv[2]
        SAVE_PATH = sys.argv[3]
    except:
        print('supply parameters as said in usage')
        sys.exit()

    dirTree = walk(PATH_TO_IMAGES)

    for tup in dirTree:
        for filename in tup[2]:
            imgPath = tup[0] + "\\" + filename

            image = load(imgPath, cv2.IMREAD_COLOR)

            if image is None:
                print('Error - Nepodrzan format slike')
                print('Podrzani formati:\n.bmp, .dib, .jpeg, .jpg, .jpe, .jp2, .png, .webp, .pbm, .pgm, .ppm, .sr, .ras, .tiff, .tif')
                continue

            print("Successfully loaded image " + filename)

            image = cv2.resize(image, (0,0), fx=FACTOR, fy=FACTOR, interpolation=cv2.INTER_AREA)
            
            save(SAVE_PATH + '\\' + filename, image)
            print('OK - image resized: ' + filename)