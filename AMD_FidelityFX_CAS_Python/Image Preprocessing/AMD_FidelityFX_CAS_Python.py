# -*- coding: utf-8 -*-
import cv2
import numpy as np
import math
import sys
import multiprocessing as mp
from os import walk, getenv, mkdir

# a - lower limit
# b - upper limit
# c - lerp value in range [0.0, 1.0]
# When c = 0 returns a.
# When c = 1 return b.
# When c = 0.5 returns the midpoint of a and b.
def lerp(a, b, c):
    return (b * c + (-a * c + a))

# Clamps value between 0 and 1 and returns value. 
# ASatF1 from AMD FidelityFX header
def clamp01(x):
    return min(1.0, max(0.0, x))

# performs math operation => 1.0/x
def rcp(x):
    return (1.0 / x)

# Applies AMD FidelityFX CAS filter on the whole image pixel by pixel
# srcImage is the source image
# sharpness - float value ranging from [0.0, 1.0] adjusting the sharpness of the image
# returns image filtered via the CAS algorithm
def casFilter(srcImg, sharpness):
    # data conversion
    srcImg = srcImg.astype(np.float32)
    srcImg /= 255.0 # convert values from [0.0, 255.0] to [0.0, 1.0]

    outImg = np.zeros(srcImg.shape, dtype=np.float32)
    srcImgExt = extendImageByOne(srcImg)

    sharp = -rcp(lerp(8.0, 5.0, clamp01(sharpness)))

    for i in range(0, srcImg.shape[0]):
        for j in range(0, srcImg.shape[1]):
            imgPart = srcImgExt[i : i+3, j : j+3]

            # Soft min and max.
            #  a b c             b
            #  d e f * 0.5  +  d e f * 0.5
            #  g h i             h
            mnR = min(imgPart[0,1,2], imgPart[1,1,2], imgPart[2,1,2], imgPart[1,0,2], imgPart[1,2,2])
            mnG = min(imgPart[0,1,1], imgPart[1,1,1], imgPart[2,1,1], imgPart[1,0,1], imgPart[1,2,1])
            mnB = min(imgPart[0,1,0], imgPart[1,1,0], imgPart[2,1,0], imgPart[1,0,0], imgPart[1,2,0])
            mnR2 = min(mnR, imgPart[0,0,2], imgPart[2,0,2], imgPart[0,2,2], imgPart[2,2,2]);
            mnG2 = min(mnG, imgPart[0,0,1], imgPart[2,0,1], imgPart[0,2,1], imgPart[2,2,1]);
            mnB2 = min(mnB, imgPart[0,0,0], imgPart[2,0,0], imgPart[0,2,0], imgPart[2,2,0]);

            mnR += mnR2
            mnG += mnG2
            mnB += mnB2

            mxR = max(imgPart[0,1,2], imgPart[1,1,2], imgPart[2,1,2], imgPart[1,0,2], imgPart[1,2,2])
            mxG = max(imgPart[0,1,1], imgPart[1,1,1], imgPart[2,1,1], imgPart[1,0,1], imgPart[1,2,1])
            mxB = max(imgPart[0,1,0], imgPart[1,1,0], imgPart[2,1,0], imgPart[1,0,0], imgPart[1,2,0])
            mxR2 = max(mnR, imgPart[0,0,2], imgPart[2,0,2], imgPart[0,2,2], imgPart[2,2,2]);
            mxG2 = max(mnG, imgPart[0,0,1], imgPart[2,0,1], imgPart[0,2,1], imgPart[2,2,1]);
            mxB2 = max(mnB, imgPart[0,0,0], imgPart[2,0,0], imgPart[0,2,0], imgPart[2,2,0]);

            mxR += mxR2
            mxG += mxG2
            mxB += mxB2

            # Smooth minimum distance to signal limit divided by smooth max.
            rcpMR = rcp(mxR)
            rcpMG = rcp(mxG)
            rcpMB = rcp(mxB)

            ampR = clamp01(min(mnR, 2.0 - mxR) * rcpMR);
            ampG = clamp01(min(mnG, 2.0 - mxG) * rcpMG);
            ampB = clamp01(min(mnB, 2.0 - mxB) * rcpMB);

            # Shaping amount of sharpening.
            ampR = math.sqrt(ampR);
            ampG = math.sqrt(ampG);
            ampB = math.sqrt(ampB);

            # Filter shape (Kernel shape).
            #  0 w 0
            #  w 1 w
            #  0 w 0
            peak = sharp;
            wR = ampR * peak;
            wG = ampG * peak;
            wB = ampB * peak;

            # Filter.
            rcpWeightR = rcp(1.0 + 4.0 * wR);
            rcpWeightG = rcp(1.0 + 4.0 * wG);
            rcpWeightB = rcp(1.0 + 4.0 * wB);

            kernel = np.array([[[0,0,0],
                               [wB,wG,wR],
                               [0,0,0]],
                              [[wB,wG,wR],
                               [1,1,1],
                               [wB,wG,wR]],
                              [[0,0,0],
                               [wB,wG,wR],
                               [0,0,0]]], dtype=np.float32)

            kernel[:,:,0] *= rcpWeightB
            kernel[:,:,1] *= rcpWeightG
            kernel[:,:,2] *= rcpWeightR

            for k in range(0, kernel.shape[0]):
                for l in range(0, kernel.shape[1]):
                    outImg[i, j] += (srcImgExt[i+k, j+l] * kernel[k, l]) # clamp values from 0.0 to 1.0 and add to outImg
                    
            outImg[i, j] = outImg[i, j].clip(0.0, 1.0)

    outImg *= 255.0
    outImg = outImg.astype(np.uint8)
    return outImg

# Applies AMD FidelityFX CAS filter on the whole image asynchronously
# srcImage is the source image
# sharpness - float value ranging from [0.0, 1.0] adjusting the sharpness of the image
# returns image filtered via the CAS algorithm
def casFilter_async(srcImg, sharpness):
    # data conversion
    srcImg = srcImg.astype(np.float32)
    srcImg /= 255.0 # convert values from [0.0, 255.0] to [0.0, 1.0]

    outImg = np.zeros(srcImg.shape, dtype=np.float32)
    srcImgExt = extendImageByOne(srcImg) # get extended image

    results = []
    def collect_result(result):
        results.append(result)

    sharp = -rcp(lerp(8.0, 5.0, clamp01(sharpness)))

    imagePieces = splitImage(srcImgExt, mp.cpu_count()) # split extended source image into parts by amount of cpu cores

    pool = mp.Pool(mp.cpu_count()) # initialize multiprocess Pool with cpu count

    for piece in imagePieces:
        pool.apply_async(casFilterJob, args=(piece, sharp), callback=collect_result)

    pool.close()
    pool.join()  # postpones the execution of next line of code until all processes in the queue are done.

    print("Joining processed image pieces into final image")
    outImg = joinImage(results, srcImg.shape)
    
    return outImg

# async job that processes one piece of an image using the cas filter
def casFilterJob(imgPiece, sharpness):
    img = imgPiece[0]
    height = img.shape[0] - 2
    width = img.shape[1] - 2
    depth = img.shape[2]

    outImg = np.zeros((height, width, depth), dtype=np.float32)

    for i in range(0, height):
        for j in range(0, width):
            imgPart = img[i : i+3, j : j+3]

            # Soft min and max.
            #  a b c             b
            #  d e f * 0.5  +  d e f * 0.5
            #  g h i             h
            mnR = min(imgPart[0,1,2], imgPart[1,1,2], imgPart[2,1,2], imgPart[1,0,2], imgPart[1,2,2])
            mnG = min(imgPart[0,1,1], imgPart[1,1,1], imgPart[2,1,1], imgPart[1,0,1], imgPart[1,2,1])
            mnB = min(imgPart[0,1,0], imgPart[1,1,0], imgPart[2,1,0], imgPart[1,0,0], imgPart[1,2,0])
            mnR2 = min(mnR, imgPart[0,0,2], imgPart[2,0,2], imgPart[0,2,2], imgPart[2,2,2]);
            mnG2 = min(mnG, imgPart[0,0,1], imgPart[2,0,1], imgPart[0,2,1], imgPart[2,2,1]);
            mnB2 = min(mnB, imgPart[0,0,0], imgPart[2,0,0], imgPart[0,2,0], imgPart[2,2,0]);

            mnR += mnR2
            mnG += mnG2
            mnB += mnB2

            mxR = max(imgPart[0,1,2], imgPart[1,1,2], imgPart[2,1,2], imgPart[1,0,2], imgPart[1,2,2])
            mxG = max(imgPart[0,1,1], imgPart[1,1,1], imgPart[2,1,1], imgPart[1,0,1], imgPart[1,2,1])
            mxB = max(imgPart[0,1,0], imgPart[1,1,0], imgPart[2,1,0], imgPart[1,0,0], imgPart[1,2,0])
            mxR2 = max(mnR, imgPart[0,0,2], imgPart[2,0,2], imgPart[0,2,2], imgPart[2,2,2]);
            mxG2 = max(mnG, imgPart[0,0,1], imgPart[2,0,1], imgPart[0,2,1], imgPart[2,2,1]);
            mxB2 = max(mnB, imgPart[0,0,0], imgPart[2,0,0], imgPart[0,2,0], imgPart[2,2,0]);

            mxR += mxR2
            mxG += mxG2
            mxB += mxB2

            # Smooth minimum distance to signal limit divided by smooth max.
            rcpMR = rcp(mxR)
            rcpMG = rcp(mxG)
            rcpMB = rcp(mxB)

            ampR = clamp01(min(mnR, 2.0 - mxR) * rcpMR);
            ampG = clamp01(min(mnG, 2.0 - mxG) * rcpMG);
            ampB = clamp01(min(mnB, 2.0 - mxB) * rcpMB);

            # Shaping amount of sharpening.
            ampR = math.sqrt(ampR);
            ampG = math.sqrt(ampG);
            ampB = math.sqrt(ampB);

            # Filter shape (Kernel shape).
            #  0 w 0
            #  w 1 w
            #  0 w 0
            peak = sharpness;
            wR = ampR * peak;
            wG = ampG * peak;
            wB = ampB * peak;

            # Filter.
            rcpWeightR = rcp(1.0 + 4.0 * wR);
            rcpWeightG = rcp(1.0 + 4.0 * wG);
            rcpWeightB = rcp(1.0 + 4.0 * wB);

            kernel = np.array([[[0,0,0],
                               [wB,wG,wR],
                               [0,0,0]],
                              [[wB,wG,wR],
                               [1,1,1],
                               [wB,wG,wR]],
                              [[0,0,0],
                               [wB,wG,wR],
                               [0,0,0]]], dtype=np.float32)

            kernel[:,:,0] *= rcpWeightB
            kernel[:,:,1] *= rcpWeightG
            kernel[:,:,2] *= rcpWeightR

            for k in range(0, kernel.shape[0]):
                for l in range(0, kernel.shape[1]):
                    outImg[i, j] += (img[i+k, j+l] * kernel[k, l]) # clamp values from 0.0 to 1.0 and add to outImg
                    
            outImg[i, j] = outImg[i, j].clip(0.0, 1.0)

    return (outImg, imgPiece[1])

# Splits the image into numOfSplits*numOfSplits pieces
# srcImage - the image to be split
# numOfSplits - the number of splits to perform
def splitImage(srcImg, numOfSplits):
    height = srcImg.shape[0]
    width = srcImg.shape[1]

    heightSplitSize = int(height / numOfSplits);
    widthSplitSize = int(width / numOfSplits);

    imagePieces = []

    for r in range(1, height - 1, heightSplitSize):
        for c in range(1, width - 1, widthSplitSize):
            imgSlice = srcImg[r - 1 : r + heightSplitSize + 1, c - 1 : c + widthSplitSize + 1]

            if(imgSlice.shape[0] <= 2 or imgSlice.shape[1] <= 2):
                continue

            hStart = r-1
            hEnd = hStart + imgSlice.shape[0] - 2
            wStart = c-1
            wEnd = wStart + imgSlice.shape[1] - 2
            imagePieces.append((imgSlice, [hStart, hEnd, wStart, wEnd]))

    return imagePieces

# Joins the resulting image slices together
# results - the results from casFilterJobs
# imageShape - the shape of the original image (height, width, depth)
def joinImage(results, imgShape):
    compImg = np.zeros(imgShape, dtype=np.float32)
    
    for result in results:
        imgLimits = result[1]
        compImg[imgLimits[0]:imgLimits[1],imgLimits[2]:imgLimits[3]] = result[0]

    compImg *= 255.0
    compImg = compImg.astype(np.uint8)
    return compImg

# extends image by one from all sides
def extendImageByOne(image):
    newImg = np.zeros((image.shape[0] + 2, image.shape[1] + 2, image.shape[2]), dtype=np.float32)
    newImg[1:-1, 1:-1] = image

    newImg[1:-1, 0] = image[:, 0] # extend top
    newImg[1:-1, -1] = image[:, -1] # extend bottom
    newImg[0, 1:-1] = image[0, :] # extend left
    newImg[-1, 1:-1] = image[-1, :] # extend right

    newImg[0, 0] = image[0, 0] # top left corner
    newImg[-1, 0] = image[-1, 0] # top right corner
    newImg[0, -1] = image[0, -1] # bottom left corner
    newImg[-1, -1] = image[-1, -1] # bottom right corner

    return newImg

# saves image to save path with given name
def save(path, image):
    print("Saving image to: " + path)
    cv2.imwrite(path, image)

# loads an image from path
def load(path, flag):
    return cv2.imread(path, flag)

if __name__ == '__main__':
    print("Beginning application of AMD FidelityFX CAS filtering.")
    if(len(sys.argv) < 4):
        print('usage: python AMD_FidelityFX_CAS_Python.py <sharpness from [0.0, 1.0]> "absolute path do slikama" "absolute path za spremanje novih slika"')
        sys.exit()

    try:
        SHARPNESS = float(sys.argv[1])
        PATH_TO_IMAGES = sys.argv[2]
        SAVE_PATH = sys.argv[3]
    except:
        print('supply parameters as said in usage')
        sys.exit()

    dirTree = walk(PATH_TO_IMAGES)
    print("Sharpness value: {0}".format(SHARPNESS))

    for tup in dirTree:
        for filename in tup[2]:
            imgPath = tup[0] + "\\" + filename

            image = load(imgPath, cv2.IMREAD_COLOR)

            if image is None:
                print('Error - Nepodrzan format slike')
                print('Podrzani formati:\n.bmp, .dib, .jpeg, .jpg, .jpe, .jp2, .png, .webp, .pbm, .pgm, .ppm, .sr, .ras, .tiff, .tif')
                continue

            print("Successfully loaded image " + filename)

            # apply CAS filter
            filteredImage = casFilter_async(image, SHARPNESS)
            print("Completed casFilter")

            save(SAVE_PATH + '\\' + filename, filteredImage)
            print('OK - image filtered: ' + filename)
