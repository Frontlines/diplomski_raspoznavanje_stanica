# -*- coding: utf-8 -*-
import cv2
import numpy as np
import sys
from os import walk, getenv, mkdir

# saves image to save path with given name
def save(path, image):
    print("Saving image to: " + path)
    cv2.imwrite(path, image)

# loads an image from path
def load(path, flag):
    return cv2.imread(path, flag)

if(len(sys.argv) < 3):
    print('usage: python ContrastEnhance.py "direktorij sa slikama" "direktorij za spremanje"')
    sys.exit()

try:
    PATH_TO_IMAGES = sys.argv[1]
    SAVE_PATH = sys.argv[2]
except:
    print('supply parameters as said in usage')
    sys.exit()

dirTree = walk(PATH_TO_IMAGES)

for tup in dirTree:
    for filename in tup[2]:
        imgPath = tup[0] + "\\" + filename

        image = load(imgPath, cv2.IMREAD_COLOR)

        if image is None:
            print('Error - Nepodrzan format slike')
            print('Podrzani formati:\n.bmp, .dib, .jpeg, .jpg, .jpe, .jp2, .png, .webp, .pbm, .pgm, .ppm, .sr, .ras, .tiff, .tif')
            continue

        print("Successfully loaded image " + filename)

        # Converting image to LAB Color model
        lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

        # Splitting the LAB image to different channels
        l, a, b = cv2.split(lab)

        # Applying CLAHE to L-channel
        clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
        cl = clahe.apply(l)

        # Merge the CLAHE enhanced L-channel with the a and b channel
        limg = cv2.merge((cl,a,b))

        # Converting image from LAB Color model to RGB model
        image = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

        save(SAVE_PATH + '\\' + filename, image)

        print('Image saved as ' + filename)

