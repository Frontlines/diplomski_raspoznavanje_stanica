# PREPOZNAVANJE KRVNIH STANICA POMOĆU DUBOKIH NEURONSKIH MREŽA

## Opis zadatka

Zadatak ovog diplomskog rada je implementacija i treniranje duboke neuronske mreze za prepoznavanje tipa krvnih stanica na slikama krvnih razmaza.
Za treniranje bi se dijelom koristio vlastiti dataset prikupljen u Klinickoj bolnici Osijek, a dijelom javno dostupni podaci.
Prvi korak rada je treniranje neke od arhitektura duboke neuronske mreze (npr. Faster R-CNN) za detekciju i klasifikaciju bijelih i crvenih krvnih stanica.
Dva su pristupa klasifikaciji bijelih krvnih stanica: transfer learning (dodavanje novih klasa objekata dubokoj neuronskoj mrezi iz prvog koraka i retreniranje)
ili treniranje nove arhitekture duboke neuronske mreze. Razvijene metode treba testirati na ranije odvojenom setu slika (10-15% originalnog dataseta).

## Opis (tijek) implementacije
### Stvaranje dataseta

Skripte za tansformaciju se nalaze u [AMD_FidelityFX_CAS_Python](https://gitlab.com/Frontlines/diplomski_raspoznavanje_stanica/-/tree/master/AMD_FidelityFX_CAS_Python) projektu.

Izraditi će mo dva dataseta: 
 * dataset za prepoznavanje krvnih stanica (crvene krvne stanice, bijele krvne stanice i trombociti)
 * dataset za prepoznavanje bijelih krvnih stanica (neutrofil, limfocita, eozinofil, monocita, bazofil)
 
Prvo je potrebno označiti sve značajke na slikama krvnih razmaza. Gore je navedeno o kojim značajkama se radi.
Označavanje slika sam odradio pomoću jednog open source alata za označavanje značajki, ilitiga bounding boxeva: [LabelImg](https://msdn.microsoft.com/en-us/library/windows/desktop/jj863687.aspx)
Značajke sam spremao u YOLO formatu, jer YOLO sprema podatke o bounding boxeva u normaliziranom formatu (rasponu od 0.0-1.0),
time kada se promjene dimenzije slike - dimenzije bounding box-eva ostaju valjane.

S obzirom da su originalne slike poprilično velikih dimenzija (2560x1920) i da su spremljene u formatu .tif koji ima jako mal stupanj kompresije (ili ga nema),
svaka od tih slika poprima 14MB prostora (a ima oko 200 primjeraka) - to je gotovo 3GB prostora.
Kako bi dataset zauzimao manje prostora, bio lakše upravljiv i ubrzao proces uploadanja dataseta (slika),
zaključujem da je potrebno resize-at slike i spremit ih u format koji pruža veći stupanj kompresije ali ne gubi previše u kvaliteti slike.

Slike potom resize-am na rezoluciju 800x600 i spremam u .png format.

Idući korak je prijelaz filterima preko svih slika.
 * prvi filtar kojim prelazim je Median Filter - razlog je uklanjanje bilo kakvog šuma na slikama ako se slučajno našo
 * drugi filtar je [AMD FidelityFX CAS](https://github.com/GPUOpen-Effects/FidelityFX-CAS) Filter (CAS - Contrast Adaptive Sharpening) - raspisao sam vlastitu implementaciju za CPU
 
Nakon filtriranje, slike sam uploadao na Roboflow - servis za generiranje datasetova sa raznim postavkama za preprocessing ili za odrađivanje augmentacija slika kako bi se proširio dataset.
Odrađivanjem augmentacija slika proširuje nam se dataset i dokazano je da augmentacije poboljšavaju rezultate treniranja.
Datasetovi koje ćemo mi koristit će mo naknadno još smanjiti na 480x480 i odraditi augmentacija Flip Horizontal, Rotation -20/+20 stupnja i Shear +/- 10 horizontalno i vertikalno.

Time imamo izgenerirane datasetova.

### Izrada Faster R-CNN modela

Unutar projekta [FasterRCNN_Python](https://gitlab.com/Frontlines/diplomski_raspoznavanje_stanica/-/tree/master/FasterRCNN_Python) se nalaze sve skripte potrebne za treniranje modela
za detekciju krvnih stanica, odnosno bijelih krvnih stanica. Treniranje i generiranje modela Faster R-CNN modela je napisana pomoću PyTorch biblioteke.

Pod direktorijem colab se nalazi .ipynb datoteka koja se može učitati u Google Colab servisu, te se tamo može provrtiti treniranje (treba postaviti da se koristi GPU runtime za brže treniranje)

Treniranje je bilo poprilično sporo pa sam provrtio samo oko 200 koraka za treniranje detekcije krvnih stanica i 100 koraka za detekciju bijelih, time da sam primjenio transfer learning,
koristeći prijašnje istreniran model za detekciju krvnih stanica i smrzo oko 80% slojeva za detekciju značajki u CNN-u.

Link na istrenirane modele: https://drive.google.com/drive/folders/1mBOLiBIkV4qQVpancXS4LJ8ZRmI8BM_-?usp=sharing

### Aplikacija

Projekt [aplikacije](https://gitlab.com/Frontlines/diplomski_raspoznavanje_stanica/-/tree/master/RaspoznavanjeStanica_App)

Aplikacija koja koristi istrenirane modele kako bi provela identifikaciju stanica na skupu slika je izrađena koristeći C# i WPF framework.
Kako bi se mogao koristi model u aplikaciji, potrebno je spremiti PyTorch model u .onnx format. Onnx je otvoreni standard za podjelu ML modela.
Potom se novi .onnx model može učitati pomoću ML.NET frameworka za strojno učenje u .NET okruženju.

** NAPOMENA**
Prije korištenja aplikacije potrebno je u Assets/Models folder projekta dodati .onnx modele za detekciju krvnih stanica i bijelih krvnih stanica.
Nakon dodavanja fajlova, treba na svaki od novo-dodanih fajlova kliknuti i u Properties tabu postaviti Build Action na Content i Copy to Ouput Directory na Copy if newer.
Potom je potrebno ažurirati pathove do novo-dodanih fajlova u MainWindowViewModel.cs skripti. Uglavnom se sam treba promijeniti naziv datoteke za svaki model.

