﻿using System.ComponentModel;
using PropertyChanged;

namespace RaspoznavanjeStanica_App.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };
    }
}
