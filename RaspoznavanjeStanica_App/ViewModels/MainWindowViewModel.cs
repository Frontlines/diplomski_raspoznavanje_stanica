﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Printing;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Command;
using Microsoft.ML;
using RaspoznavanjeStanica_App.FasterRcnnParser;
using RaspoznavanjeStanica_App.Models;
using RaspoznavanjeStanica_App.Utilities;
using Path = System.IO.Path;

namespace RaspoznavanjeStanica_App.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Enums

        public enum Detector { Cell, WBC, None }

        #endregion

        #region StaticPathStrings

        private static readonly string _assetsRelativePath = @".\Assets";
        private static readonly string _cellsModelPath = Path.Combine(_assetsRelativePath, "Models", "cellsModel.onnx");
        private static readonly string _wbcModelPath = Path.Combine(_assetsRelativePath, "Models", "wbcModel.onnx");

        #endregion

        #region ViewBoundProperties

        public string SavePath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public string ImagesPath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
        public BitmapSource ActiveImage { get; set; }
        public Detector DetectorOption { get; set; } = Detector.Cell;
        public float ScoreThreshold { get; set; } = 0.8f;
        public int FontSize { get; set; } = 24;
        public int LineThickness { get; set; } = 12;
        public ObservableCollection<ImageData> ImageDatas { get; set; }
        public int SelectedImageIndex { get; set; }
        public string Logs { get; set; }

        #endregion

        #region MLNET

        // core object when using ML.NET functionalities 
        private MLContext _mlContext = new MLContext();

        private string ModelPath => DetectorOption == Detector.Cell ? _cellsModelPath : _wbcModelPath;

        #endregion

        #region Misc

        private FolderBrowserDialog _imagesDialog = new FolderBrowserDialog();
        private FolderBrowserDialog _savePathDialog = new FolderBrowserDialog();

        public int TotalCellCount { get; set; }

        #endregion

        public MainWindowViewModel()
        {
            _savePathDialog.Description = "Processed Image Save Location";
            _savePathDialog.ShowNewFolderButton = true;
            _savePathDialog.SelectedPath = SavePath;

            _imagesDialog.Description = "Images Directory";
            _imagesDialog.ShowNewFolderButton = false;
            _imagesDialog.SelectedPath = ImagesPath;

            ImageDatas = new ObservableCollection<ImageData>(ImageData.ReadFromFile(ImagesPath, SavePath).ToArray());
            if (ImageDatas.Any())
            {
                SelectedImageIndex = 0;
            }
        }

        #region Commands

        public RelayCommand OpenImagesCommand => new RelayCommand(() =>
        {
            var result = _imagesDialog.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(_imagesDialog.SelectedPath))
            {
                ImagesPath = _imagesDialog.SelectedPath;
            }

            ImageDatas = new ObservableCollection<ImageData>(ImageData.ReadFromFile(ImagesPath, SavePath).ToArray());
            if (ImageDatas.Any())
            {
                SelectedImageIndex = 0;
            }

            Logs += "Image/s Open - Success\n";

            Preview();
        });
        public RelayCommand ClearLogsCommand => new RelayCommand(() => { Logs = ""; });
        public RelayCommand PickSavePathCommand => new RelayCommand(() =>
        {
            var result = _savePathDialog.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(_savePathDialog.SelectedPath))
            {
                SavePath = _savePathDialog.SelectedPath;
            }
        });
        public RelayCommand ProcessImagesCommand => new RelayCommand(ProcessAllImages);
        public RelayCommand PreviewCommand => new RelayCommand(()=>
        {
            Preview(SelectedImageIndex);
        });

        #endregion

        // generates a prediction for a single file and displays it in the app
        private void Preview()
        {
            if (!ImageDatas.Any())
            {
                Logs += "No compatible images found in selected directory.\n";
                return;
            }

            var images = ImageDatas.Take(1).ToArray();

            Predict(images, true);
        }

        // generates a prediction for a specific image at given path
        private void Preview(int imageIndex)
        {
            var image = ImageDatas.ElementAtOrDefault(imageIndex);

            if (image == null)
            {
                Logs += "No such element in collection at given index.\n";
                return;
            }

            Predict(new ImageData[] { image }, true);
        }

        // goes through all the loaded images in the given directory and generates predictions and saves them to save path
        private void ProcessAllImages()
        {
            if (!ImageDatas.Any())
            {
                Logs += "No compatible images found in selected directory.\n";
                return;
            }

            Predict(ImageDatas.ToArray());
        }

        private void Predict(ImageData[] images, bool preview = false)
        {
            IDataView imageDataView = _mlContext.Data.LoadFromEnumerable(images);

            var modelScorer = new OnnxModelScorer(ImagesPath, ModelPath, _mlContext);

            // Use model to score data
            (IEnumerable<float[]>, IEnumerable<long[]>, IEnumerable<float[]>) predictions = modelScorer.Score(imageDataView);

            IEnumerable<IEnumerable<ObjectDetectionPrediction>> imagePredictions;
            if (DetectorOption == Detector.Cell)
            {
                imagePredictions = FasterRcnnOutputParser.Parse<CellPrediction>(predictions, ScoreThreshold);
            }
            else
            {
                imagePredictions = FasterRcnnOutputParser.Parse<WbcPrediction>(predictions, ScoreThreshold);
            }

            TotalCellCount = 0;
            int indx = 0;
            foreach (var imagePrediction in imagePredictions)
            {
                Logs += $"Drawing prediction boxes for image: {images[indx].ImagePath}\n";
                var editor = DrawBoxes(imagePrediction, images[indx].ImagePath, images[indx].SavePath);

                if (preview)
                {
                    ActiveImage = editor.Preview();
                }
                else
                {
                    editor.Save();
                }
                editor.Dispose();
                indx++;
            }

            Logs += $"Total Cells Detected: {TotalCellCount}";
        }

        // draws predicted bounding boxes with labels on the input images
        private ImageEditor DrawBoxes(IEnumerable<ObjectDetectionPrediction> cellPredictions, string inputFile, string outputFile)
        {
            var editor = new ImageEditor(inputFile, outputFile, fontSize: FontSize);

            foreach (var prediction in cellPredictions)
            {
                Logs += $"Drawing box: {prediction.BoundingBox}\n";
                editor.AddBox(prediction, LineThickness);
                TotalCellCount++;
            }

            return editor;
        }
    }
}
