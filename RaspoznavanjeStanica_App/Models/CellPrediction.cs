﻿using System.Drawing;

namespace RaspoznavanjeStanica_App.Models
{
    public class CellPrediction : ObjectDetectionPrediction
    {
        private static readonly string[] LabelNames = new string[]
        {
            "BG", "RBC", "WBC", "Platelet"
        };

        private static readonly Color[] ClassColors = new Color[]
        {
            Color.Khaki,
            Color.Fuchsia,
            Color.Silver,
            Color.RoyalBlue,
        };

        public override string Name => LabelNames[Label];
        public override Color BoxColor => ClassColors[Label];

        public CellPrediction()
        {

        }

        public CellPrediction(float[] boundingBox, long label, float score) : base(boundingBox, label, score)
        {
        }
    }
}
