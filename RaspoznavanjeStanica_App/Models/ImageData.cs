﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.ML.Data;

namespace RaspoznavanjeStanica_App.Models
{
    public class ImageData
    {
        [LoadColumn(0)]
        public string ImagePath;

        public string SavePath;

        [LoadColumn(1)]
        public string Label;

        public static IEnumerable<ImageData> ReadFromFile(string imageFolder, string saveFolder)
        {
            var ext = new List<string> { ".jpg", ".png" };

            return Directory.EnumerateFiles(imageFolder, "*.*", SearchOption.TopDirectoryOnly)
                .Where(s => ext.Contains(Path.GetExtension(s).ToLowerInvariant()))
                .Select(f => new ImageData
                {
                    ImagePath = f,
                    SavePath = Path.Combine(saveFolder, Path.GetFileNameWithoutExtension(f) + "_det" + Path.GetExtension(f)),
                    Label = Path.GetFileName(f)
                });
        }

        public override string ToString()
        {
            return Label;
        }
    }
}
