﻿using System;

namespace RaspoznavanjeStanica_App.Models
{
    public struct BoundingBox
    {
        public float Xmin { get; set; }
        public float Xmax { get; set; }
        public float Ymin { get; set; }
        public float Ymax { get; set; }
        public float Height => Math.Max(0f, Ymax - Ymin);
        public float Width => Math.Max(0f, Xmax - Xmin);

        public BoundingBox(float xmin, float ymin, float xmax, float ymax)
        {
            Xmin = xmin;
            Ymin = ymin;
            Xmax = xmax;
            Ymax = ymax;
        }

        public BoundingBox(float[] boxCorners)
        {
            if (boxCorners.Length < 4 || boxCorners.Length > 4)
            {
                throw new Exception(
                    "Box array is too long or too short. The array must contain exactly 4 elements: [xmin,ymin,xmax,ymax].");
            }

            Xmin = boxCorners[0];
            Ymin = boxCorners[1];
            Xmax = boxCorners[2];
            Ymax = boxCorners[3];
        }

        public override string ToString()
        {
            return $"xmin:{Xmin} ymin:{Ymin} xmax:{Xmax} ymax:{Ymax} || Height:{Height} Width:{Width}";
        }
    }
}
