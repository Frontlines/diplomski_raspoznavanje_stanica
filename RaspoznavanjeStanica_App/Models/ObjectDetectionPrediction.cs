﻿using System.Drawing;
using System.Drawing.Imaging;
using RaspoznavanjeStanica_App.Models;

namespace RaspoznavanjeStanica_App.Models
{
    public abstract class ObjectDetectionPrediction
    {
        public BoundingBox BoundingBox { get; set; }
        public long Label { get; set; }
        public float Score { get; set; }
        public abstract string Name { get; }
        public abstract Color BoxColor { get; }

        protected ObjectDetectionPrediction()
        {

        }

        protected ObjectDetectionPrediction(float[] boundingBox, long label, float score)
        {
            BoundingBox = new BoundingBox(boundingBox);
            Label = label;
            Score = score;
        }
    }
}
