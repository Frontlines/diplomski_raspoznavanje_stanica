﻿using System.Drawing;

namespace RaspoznavanjeStanica_App.Models
{
    public class WbcPrediction : ObjectDetectionPrediction
    {
        private static readonly string[] LabelNames = new string[]
        {
            "BG", "Neutrophil", "Lymphocyte", "Eosinophil", "Monocyte", "Basophil"
        };

        private static readonly Color[] ClassColors = new Color[]
        {
            Color.Khaki,
            Color.Fuchsia,
            Color.Silver,
            Color.RoyalBlue,
            Color.Green,
            Color.DarkOrange
        };

        public override string Name => LabelNames[Label];
        public override Color BoxColor => ClassColors[Label];

        public WbcPrediction()
        {

        }

        public WbcPrediction(float[] boundingBox, long label, float score) : base(boundingBox, label, score)
        {

        }
    }
}
