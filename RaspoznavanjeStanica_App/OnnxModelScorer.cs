﻿using System.Collections.Generic;
using Microsoft.ML;
using Microsoft.ML.Data;
using RaspoznavanjeStanica_App.Models;

namespace RaspoznavanjeStanica_App
{
    public class OnnxModelScorer
    {
        private readonly string modelLocation;
        private readonly MLContext mlContext;
        
        public struct ImageSettings
        {
            public const int imageWidth = 480;
            public const int imageHeight = 480;
        }

        //input_names = ['image_in'],   # the model's input names
        //output_names = ['bboxes', 'labels', 'scores'] # the model's output names
        public struct FasterRcnnModelSettings
        {
            // input tensor name
            public const string ModelInput = "image_in";

            // output bbox tensor name
            public const string ModelBboxes = "bboxes";
            // output lbl tensor name
            public const string ModelLabels = "labels";
            // output score tensor name
            public const string ModelScores = "scores";
        }

        public OnnxModelScorer(string imagesFolder, string modelLocation, MLContext mlContext)
        {
            this.modelLocation = modelLocation;
            this.mlContext = mlContext;
        }

        private ITransformer LoadModel(string modelLocation)
        {
            var data = mlContext.Data.LoadFromEnumerable(new List<ImageData>());

            var pipeline = mlContext.Transforms.LoadImages(outputColumnName: "image_in", imageFolder: "",
                    inputColumnName: nameof(ImageData.ImagePath))
                .Append(mlContext.Transforms.ResizeImages(outputColumnName: "image_in", inputColumnName: "image_in",
                    imageWidth: ImageSettings.imageWidth, imageHeight: ImageSettings.imageHeight))
                .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "image_in"))
                .Append(mlContext.Transforms.ApplyOnnxModel(modelFile: modelLocation,
                    outputColumnNames: new[]
                    {
                        FasterRcnnModelSettings.ModelBboxes,
                        FasterRcnnModelSettings.ModelLabels,
                        FasterRcnnModelSettings.ModelScores
                    }, inputColumnNames: new[] {FasterRcnnModelSettings.ModelInput}));

            var model = pipeline.Fit(data);

            return model;
        }

        private (IEnumerable<float[]>, IEnumerable<long[]>, IEnumerable<float[]>) PredictDataUsingModel(IDataView testData, ITransformer model)
        {
            IDataView scoredData = model.Transform(testData);

            IEnumerable<float[]> bboxes = scoredData.GetColumn<float[]>(FasterRcnnModelSettings.ModelBboxes);
            IEnumerable<long[]> labels = scoredData.GetColumn<long[]>(FasterRcnnModelSettings.ModelLabels);
            IEnumerable<float[]> scores = scoredData.GetColumn<float[]>(FasterRcnnModelSettings.ModelScores);

            return (bboxes, labels, scores);
        }

        public (IEnumerable<float[]>, IEnumerable<long[]>, IEnumerable<float[]>) Score(IDataView data)
        {
            var model = LoadModel(modelLocation);

            return PredictDataUsingModel(data, model);
        }

    }
}
