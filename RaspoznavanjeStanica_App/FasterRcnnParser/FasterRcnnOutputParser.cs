﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using RaspoznavanjeStanica_App.Models;

namespace RaspoznavanjeStanica_App.FasterRcnnParser
{
    public static class FasterRcnnOutputParser
    {
        public static IEnumerable<IEnumerable<T>> Parse<T>(
            (IEnumerable<float[]>, IEnumerable<long[]>, IEnumerable<float[]>) predictions, float scoreThreshold) where T : ObjectDetectionPrediction, new()
        {
            List<List<T>> perImagePredictions = new List<List<T>>();

            var zipLabelScore = predictions.Item2.Zip(predictions.Item3);
            var zippedPredictions = zipLabelScore.Zip(predictions.Item1);

            // This foreach iterates through the predictions of each image that was passed for prediction to the onnx model.
            foreach (var imgPredictions in zippedPredictions)
            {
                var labels = imgPredictions.First.First;
                var scores = imgPredictions.First.Second;
                var boundingBoxes = imgPredictions.Second;
                var numberOfPredictions = labels.Length;

                List<T> cellPredictions = new List<T>();

                // iterate through the predictions
                for (int i = 0; i < numberOfPredictions; i++)
                {
                    // if score is less then threshold, then ignore bounding box
                    if (scores[i] < scoreThreshold) continue;

                    var prediction = new T
                    {
                        BoundingBox = new BoundingBox(boundingBoxes[(i * 4)..(i * 4 + 4)]),
                        Label = labels[i],
                        Score = scores[i]
                    };
                    cellPredictions.Add(prediction);
                }

                perImagePredictions.Add(cellPredictions);
            }

            return perImagePredictions;
        }
    }
}
