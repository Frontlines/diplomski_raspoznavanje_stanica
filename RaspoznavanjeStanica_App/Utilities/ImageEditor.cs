﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;
using RaspoznavanjeStanica_App.Models;

namespace RaspoznavanjeStanica_App.Utilities
{
	/// <summary>
	/// Allows to add graphic elements to the existing image.
	/// </summary>
	public class ImageEditor : IDisposable
	{
		private Graphics _graphics;
		private Image _image;
		private string _fontFamily;
		private float _fontSize;
		private string _outputFile;

		public ImageEditor(string inputFile, string outputFile, string fontFamily = "Ariel", float fontSize = 24)
		{
			if (string.IsNullOrEmpty(inputFile))
			{
				throw new ArgumentNullException(nameof(inputFile));
			}

			if (string.IsNullOrEmpty(outputFile))
			{
				throw new ArgumentNullException(nameof(outputFile));
			}

			_fontFamily = fontFamily;
			_fontSize = fontSize;
			_outputFile = outputFile;

			_image = Bitmap.FromFile(inputFile);
			_graphics = Graphics.FromImage(_image);
		}

		/// <summary>
		/// Adds rectangle with a label in particular position of the image
		/// </summary>
		/// <param name="prediction"></param>
		/// <param name="lineWidth"></param>
		public void AddBox(ObjectDetectionPrediction prediction, int lineWidth = 12)
        {
            var xScaleFactor = (_image.Width / (float) OnnxModelScorer.ImageSettings.imageWidth);
            var yScaleFactor = (_image.Height / (float) OnnxModelScorer.ImageSettings.imageHeight);

			var left = Math.Clamp(prediction.BoundingBox.Xmin * xScaleFactor, 0f, _image.Width);
			var right = Math.Clamp(prediction.BoundingBox.Xmax * xScaleFactor, 0f, _image.Width);
			var top = Math.Clamp(prediction.BoundingBox.Ymin * yScaleFactor, 0f, _image.Height);
            var bottom = Math.Clamp(prediction.BoundingBox.Ymax * yScaleFactor, 0f, _image.Height);

            var imageRectangle = new Rectangle(new Point(0, 0), new Size(_image.Width, _image.Height));
			_graphics.DrawImage(_image, imageRectangle);

			Color color = prediction.BoxColor;
			Brush brush = new SolidBrush(color);
			Pen pen = new Pen(brush, lineWidth);

			_graphics.DrawRectangle(pen, left, top, right - left, bottom - top);
			var font = new Font(_fontFamily, _fontSize);
            var text = $"{prediction.Name} : {(prediction.Score * 100):0}%";
			SizeF size = _graphics.MeasureString(text, font);
			_graphics.DrawString(text, font, brush, new PointF(left, top - size.Height));
		}

        public BitmapImage Preview()
        {
            using (var memory = new MemoryStream())
            {
                _image.Save(memory, ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

        public void Save()
        {
			_image.Save(_outputFile);
        }

		public void Dispose()
		{
			if (_image != null)
			{
                if (_graphics != null)
				{
					_graphics.Dispose();
				}

				_image.Dispose();
			}
		}
	}
}
