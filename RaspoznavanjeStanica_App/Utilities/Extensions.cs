﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace RaspoznavanjeStanica_App.Utilities
{
    public static class Extensions
    {
        public static IEnumerable<Color> GetColorGradients(Color min, Color max, int numSamples)
        {
            int rMax = max.R;
            int rMin = min.R;
            int gMax = max.G;
            int gMin = min.G;
            int bMax = max.B;
            int bMin = min.B;
            
            var colorList = new List<Color>();
            for (int i = 0; i < numSamples; i++)
            {
                var rAverage = rMin + (int)((rMax - rMin) * i / numSamples);
                var gAverage = gMin + (int)((gMax - gMin) * i / numSamples);
                var bAverage = bMin + (int)((bMax - bMin) * i / numSamples);
                colorList.Add(Color.FromArgb(rAverage, gAverage, bAverage));
            }

            return colorList;
        }

        public static Color GetColorFromGradient(Color min, Color max, float sample)
        {
            int rMax = max.R;
            int rMin = min.R;
            int gMax = max.G;
            int gMin = min.G;
            int bMax = max.B;
            int bMin = min.B;

            var r = rMin + (int)((rMax - rMin) * sample);
            var g = gMin + (int)((gMax - gMin) * sample);
            var b = bMin + (int)((bMax - bMin) * sample);

            return Color.FromArgb(r, g, b);
        }

        public static string GetAbsolutePath(string relPath)
        {
            FileInfo _dataRoot = new FileInfo(typeof(App).Assembly.Location);
            string assemblyFolderPath = _dataRoot.Directory.FullName;

            string fullPath = Path.Combine(assemblyFolderPath, relPath);

            return fullPath;
        }
    }
}
